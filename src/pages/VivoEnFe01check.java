package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class VivoEnFe01check extends TestBaseTG {
	
	final WebDriver driver;
	public VivoEnFe01check(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-24']/a")
	private WebElement titulo;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-25']/a")
	private WebElement titulo2;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-151']/a")
	private WebElement titulo3;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-152']/a")
	private WebElement titulo4;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-23']/a")
	private WebElement titulo5;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-26']/a")
	private WebElement titulo6;
	
	
	//*****************************

	
	public void LogInVivoEnFe(String apuntaA) {
		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test VivoEnFe01 - Check");
		
	espera(2500);
	
	WebElement Home = driver.findElement(By.xpath("//a[contains(text(), 'HOME')]"));
	Home.click();
	
	String Titulo = titulo.getText();
		Assert.assertEquals("HOME", Titulo);
		System.out.println(Titulo);

		espera(2500);
	
	WebElement Imagenes = driver.findElement(By.xpath("//a[contains(text(), 'IMAGENES')]"));
	Imagenes.click();
	
	String Titulo2 = titulo2.getText();
	Assert.assertEquals("IMAGENES", Titulo2);
	System.out.println(Titulo2);
	
	espera(2500);
	
	WebElement Peticiones = driver.findElement(By.xpath("//a[contains(text(), 'PETICIONES')]"));
	Peticiones.click();
	
	String Titulo3 = titulo3.getText();
	Assert.assertEquals("PETICIONES", Titulo3);
	System.out.println(Titulo3);
	
	espera(2500);
	
	WebElement PildorasDeFe = driver.findElement(By.xpath("//a[contains(text(), 'PÍLDORAS DE FE')]"));
	PildorasDeFe.click();
	
	String Titulo4 = titulo4.getText();
	Assert.assertEquals("PÍLDORAS DE FE", Titulo4);
	System.out.println(Titulo4);
	
	espera(2500);
	
	WebElement Adoptame = driver.findElement(By.xpath("//a[contains(text(), 'BLOG')]"));
	Adoptame.click();
	
	String Titulo5 = titulo5.getText();
	Assert.assertEquals("BLOG", Titulo5);
	System.out.println(Titulo5);
	
	espera(2500);
	
	/*WebElement MariaVision = driver.findElement(By.xpath("//a[contains(text(), 'MARÍA VISIÓN')]"));
	MariaVision.click();
	
	String Titulo6 = titulo6.getText();
	Assert.assertEquals("MARÍA VISIÓN", Titulo6);
	System.out.println(Titulo6);
	
	espera(900);*/
	
	//valida que los menus se vean correctamente
	
			
	System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Vivo En Fe"); 
	System.out.println();
	System.out.println("Fin de Test VivoEnFe01 - Check");
		

	}		

}  

